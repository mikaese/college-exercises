import java.util.Scanner;

public class GradeCalculator
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		
		String name;
		Double total = 0.0;
		int id;
		
		System.out.print("Insert the student name: ");
		    name = sc.nextLine(); 
		
		System.out.print("Insert the student ID: TI");
		    id = sc.nextInt();
		
		for (int i=1;i<=5;i++)
		{
		    System.out.println("Insert the grade number " + i + ": ");
		    total += sc.nextFloat();  
		}
		
		total /= 5;
		System.out.print("The student " + name + ", ID number TI" + id + ", had " + total + "% of passing grade at this course.");
	}
}


#include <iostream>
#include <iomanip> 
using namespace std;

int main()
{
    char option;
    
    label:
    
        cout << "G - Green \nO - Orange \nR - Red \nWhich color is the signal: ";
        cin >> option;
        
        switch(option)
        {
            case 'R':
                cout << "Stop! You can not go.";
                break;
            
            case 'r':
                cout << "Stop! You can not go.";
                break;
                
            case 'O':
                cout << "Slow down and recheck the signal.\n" << endl;
                goto label;
             
            case 'o':
                cout << "Slow down and recheck the signal.\n" << endl;
                goto label;    
            
            case 'G': 
                label2:
                    cout << "There is any predestine in the street? (Y/N): ";
                    cin >> option;
                    
                    if (option != 'Y' && option != 'y' && option != 'N' &&  option != 'n')
                    {
                        cout << "Insert a valid option.\n";
                        goto label2;
                    }
                    else
                    { 
                        if (option == 'Y' || option == 'y')
                        {
                            cout << "Stop and wait for the predestines. \n";
                            goto label;
                        }
                        else
                        {
                            cout << "You can go!!";
                        }
                    }
                    break;
                    
            case 'g': 
                label2:
                    cout << "There is any predestine in the street? (Y/N): ";
                    cin >> option;
                    
                    if (option != 'Y' && option != 'y' && option != 'N' &&  option != 'n')
                    {
                        cout << "Insert a valid option.\n";
                        goto label2;
                    }
                    else
                    { 
                        if (option == 'Y' || option == 'y')
                        {
                            cout << "Stop and wait for the predestines. \n";
                            goto label;
                        }
                        else
                        {
                            cout << "You can go!!";
                        }
                    }
                    break;
                    
            default:
                cout << "Insert a valid option.\n" << endl;
                goto label;
        }
        
    return 0;
}
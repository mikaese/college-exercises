
#include <iostream>
#include <iomanip> 
using namespace std;

void converterCtoF(float celsius)
{
    float fahrenheit;
    
    fahrenheit = (celsius *  9 / 5) + 32;
    cout << "The temperature " << celsius << "°C in fahrenheit is: " << fixed  << setprecision(1) << fahrenheit << "°F.";
}

void converterFtoC(float fahrenheit)
{
    float celsius;
    
    celsius = (fahrenheit - 32) * 5 / 9;
    cout << "The temperature " << fahrenheit << "°F in celsius is: " << fixed  << setprecision(1) << celsius << "°C." ;
}


int main()
{
    int option;
    float t;
    
    cout << "Insert the desired option:" << endl << "1 - Convert celsius to fahrenheit." << endl << "2 - Convert fahrenheit to celsius." << endl;
    cin >> option;
    
    
    label: 
    
    switch(option)
    {
        case 1: 
            
            cout << "Insert the temperature in celsius: ";
            cin >> t;
            converterCtoF(t);
            break;
        
        case 2:
            
            cout << "Insert the temperature in fahrenheit: ";
            cin >> t;
            converterFtoC(t);
            break;
        
        default:
            
            cout << "Insert a valid option: ";
            cin >> option;
            goto label;
    }

    return 0;
}
#!/bin/bash

read -p "Enter the person name: " name 
read -p "Enter the person age: " age
read -p "Enter the person city: " city
read -p "Can $name speak french?: (Y/N) " french 

data(){
echo "Name: $name" >> output.txt 
echo "Age: $age" >> output.txt 
echo "City: $city" >> output.txt 
echo "Can speak french: $french" >> output.txt
}

if [[ "$french" != "Y" && "$french" != "y" && "$french" != "N" && "$french" != "n" ]]; then
  echo "Please put a correct option of Y or N"
  exit 1

elif [ "$age" -lt 18 ]; then
  data
  echo "Not eligible to cast the vote.
  " >> output.txt
  cat output.txt
  
elif [[ "$age" -ge 18 && "$age" -lt 21 ]]; then
  data
  echo "Ready to register for vote.
  " >> output.txt
  cat output.txt
  
else 
  data
  
  echo "Register and caste the vote." >> output.txt
  
  if [[ "$french" = "Y" || "$french" = "y" ]]; then
    echo "$name can vote in Montreal.
    " >> output.txt
  else   
    echo "$name can not vote in Montreal.
    " >> output.txt
  fi
  
  cat output.txt
  
fi   
